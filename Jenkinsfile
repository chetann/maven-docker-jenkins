#!/usr/bin/env groovy

pipeline {
    agent any
    tools {
        maven 'maven'
    }
    stages {
        stage('increment version') {
            steps {
                script {
                    echo 'incrementing app version...'
                    sh 'mvn build-helper:parse-version versions:set \
                        -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                        versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('build app') {
            steps {
               script {
                  echo 'building application jar...'
                  sh "mvn clean package"
               }
            }
        }

        stage('SonarQube analysis') {
            steps {
                withSonarQubeEnv('sonarqube-9.2.2') {
                        sh 'mvn sonar:sonar'
                }
            }
        }
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
    
        stage('build image') {
            steps {
                script {
                    echo "building the docker image..."
                    withCredentials([usernamePassword(credentialsId: 'docker-hub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh "docker build -t chetanpatil06/maven-assign:${IMAGE_NAME} ."
                        sh "echo $PASS | docker login -u $USER --password-stdin"
                        sh "docker push chetanpatil06/maven-assign:${IMAGE_NAME}"
                    }
                }
            }
        }
        stage('deploy'){
            steps{
                script{
                    def dockerCmd = 'docker run -p 9000:8080 -d chetanpatil06/maven-assign:${IMAGE_NAME}'
                    echo "Deploying the application"
                    sshagent(['ec2-server-key']) {
                        sh "ssh -o StrictHostKeyChecking=no ec2-user@13.232.170.94 ${dockerCmd}"
                    }
                }
            }
        }
        

        stage('commit version update') {
            steps {
                script {
                     withCredentials([usernamePassword(credentialsId: 'jenkins_user', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        // git config here for the first time run
                        
                       echo "Commiting the application"
                     
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/chetann/maven-docker-jenkins"

                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
